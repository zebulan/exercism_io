from collections import Counter


def detect_anagrams(original, words):
    results = list()
    orig_cnt = Counter(original.lower())
    for each in words:
        low = each.lower()  # call once below instead of twice
        if orig_cnt == Counter(low) and not original == low:
            results.append(each)
    return results
