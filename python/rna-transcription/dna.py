_rna = str.maketrans('GCTA', 'CGAU')


def to_rna(dna):
    return dna.translate(_rna)


# rna = dict(zip('GCTA', 'CGAU'))


# def to_rna(dna):
#     return ''.join(rna[letter] for letter in dna)
