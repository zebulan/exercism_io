def prime_factors(num):
    div = 2
    results = list()
    while num > 1:
        while num % div == 0:
            num /= div
            results.append(div)
        div += 1
    return results
