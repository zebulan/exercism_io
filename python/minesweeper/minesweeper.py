def board(the_board):
    new_board = list()
    height = len(the_board)
    width = len(the_board[0])
    borders = ('+', '|')
    valid_chars = {'+', '|', ' ', '*', '-'}
    to_check = ((-1, -1), (-1, 0), (-1, 1), (0, 1),
                (1, 1), (1, 0), (1, -1), (0, -1))

    for row in the_board:
        if not len(row) == width:
            raise ValueError('Invalid grid.')
        elif not row.startswith(borders) or not row.endswith(borders):
            raise ValueError('Invalid border character.')
        elif set(row).difference(valid_chars):
            raise ValueError('Invalid character(s).')
        new_board.append(list(row))

    for a in range(height):
        for b in range(width):
            if not new_board[a][b] == ' ':
                continue
            total_mines = 0
            for c, d in to_check:
                try:
                    if new_board[a + c][b + d] == '*':
                        total_mines += 1
                except IndexError:
                    pass
            if not total_mines == 0:
                new_board[a][b] = str(total_mines)
    return [''.join(row) for row in new_board]
