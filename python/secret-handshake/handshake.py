moves = ['wink', 'double blink', 'close your eyes', 'jump']


def handshake(num):
    is_int = isinstance(num, int)
    is_str = isinstance(num, str)
    if not (is_int or is_str):
        raise TypeError('Input must be integer or string!')
    elif is_int:
        if num < 1:
            return list()
        num = format(num, 'b')
    elif is_str:
        if not all(digit in ('0', '1') for digit in num):
            return list()

    results = list()
    reverse = False
    for index, digit in enumerate(reversed(num)):
        if digit == '1':
            try:
                results.append(moves[index])
            except IndexError:
                reverse = True
    return results[::-1] if reverse else results


def code(words):
    prev = None
    result = 0
    reverse = False
    for word in words:
        try:
            current = int('1' + '0' * moves.index(word))
        except ValueError:  # to check for invalid words
            return '0'
        if prev is None:    # to check first against second
            prev = current
        if current < prev:  # to check whether descending order
            reverse = True
        result += current
    return '1' + str(result) if reverse else str(result)
