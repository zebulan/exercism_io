from fractions import gcd
from math import sqrt


def is_triplet(check):
    a, b, c = sorted(check)
    return pow(a, 2) + pow(b, 2) == pow(c, 2)


def primitive_triplets(d):
    if not d % 4 == 0:
        raise ValueError('Integer must be divisible by 4!')

    by_2 = d / 2
    results = set()
    for n in range(1, int(sqrt(by_2)) + 1):
        if by_2 % n == 0:
            m = by_2 / n
            if not (m - n) % 2 == 0 and gcd(n, m) == 1:
                e = pow(m, 2) - pow(n, 2)
                results.add((min(e, d), max(e, d), pow(m, 2) + pow(n, 2)))
    return results


def triplets_in_range(start, end):
    tmp = (primitive_triplets(f) for f in range(end) if f % 4 == 0)
    prims = set()
    for triplets in tmp:
        for triplet in triplets:
            if end >= max(triplet):
                prims.add(triplet)

    results = set(prims)
    for prim in prims:
        g = 2
        while end >= max(prim) * g:
            results.add(tuple(g * h for h in prim))
            g += 1
    return set(i for i in results if start <= min(i))
