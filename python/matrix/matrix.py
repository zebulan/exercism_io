class Matrix():
    def __init__(self, nums):
        self.rows = [[int(b) for b in a.split()] for a in nums.split('\n')]
        self.columns = [list(c) for c in zip(*self.rows)]
