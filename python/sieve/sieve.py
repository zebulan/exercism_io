def gen_primes():
    """ Generate an infinite sequence of prime numbers.
        http://code.activestate.com/recipes/117119/
    """
    d = {}
    q = 2
    while True:
        if q not in d:
            yield q
            d[q * q] = [q]
        else:
            for p in d[q]:
                d.setdefault(p + q, []).append(p)
            del d[q]
        q += 1


def sieve(num):
    prime_numbers = list()
    for prime in gen_primes():
        if prime > num:
            break
        prime_numbers.append(prime)
    return prime_numbers
