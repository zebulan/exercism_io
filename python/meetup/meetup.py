from calendar import monthrange
from datetime import date


def meetup_day(yy, mm, weekday, abr):
    first_day, total_days = monthrange(yy, mm)
    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
            'Friday', 'Saturday', 'Sunday']

    # difference between first day and the dd i'm looking for
    start = (1 + (7 + (days.index(weekday) - first_day)) % 7)
    month_dates = [a for a in range(start, total_days+1, 7)]

    if abr == 'last':
        dex = -1
    elif abr == 'teenth':  # find index for 'teenth'
        for index, day in enumerate(month_dates):
            if 13 <= day <= 19:
                dex = index
                break
    else:
        convert = ['1st', '2nd', '3rd', '4th', '5th']
        dex = convert.index(abr)

    try:
        return date(yy, mm, month_dates[dex])
    except IndexError:
        raise MeetupDayException('Error. Date doesn\'t exist!')

class MeetupDayException(Exception):
    pass
