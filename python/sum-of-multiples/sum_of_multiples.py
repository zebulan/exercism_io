def sum_of_multiples(limit, multiples=(3, 5)):
    multiples = tuple(a for a in multiples if a > 0)
    results = list()
    for b in range(limit):
        for multiple in multiples:
            if b % multiple == 0:
                results.append(b)
                break
    return sum(results)
