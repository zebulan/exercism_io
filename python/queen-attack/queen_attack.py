def check_input(white, black):
    w_x, w_y = white
    b_x, b_y = black
    if white == black:
        raise ValueError('Coordinates cannot be the same.')
    elif any(not 0 <= a <= 7 for a in (w_x, w_y, b_x, b_y)):
        raise ValueError('Invalid coordinate(s).')
    return w_x, w_y, b_x, b_y


def board(white, black):
    chessboard = list()
    white_x, white_y, black_x, black_y = check_input(white, black)
    for row in range(8):
        current = list('________')
        if row == white_x:
            current[white_y] = 'W'
        elif row == black_x:
            current[black_y] = 'B'
        chessboard.append(''.join(current))
    return chessboard


def can_attack(white, black):
    white_x, white_y, black_x, black_y = check_input(white, black)
    diff_x = white_x - black_x
    diff_y = white_y - black_y
    return white_x == black_x or white_y == black_y or \
           diff_x == diff_y or diff_x * -1 == diff_y
