operations = {'plus': '+', 'minus': '-', 'multiplied': '*', 'divided': '/'}


def calculate(words):
    if not words.startswith('What is'):
        raise ValueError('Invalid question.')
    words = words.strip('?').split()[2:]  # don't need 'What is'
    statement = list()
    num_cnt = 0
    op_cnt = 0
    for word in words:
        if word.isdigit() or word.startswith('-') and word[1:].isdigit():
            statement.append(word)
            num_cnt += 1
        try:
            statement.append(operations[word])
            op_cnt += 1
        except KeyError:
            pass

    if not num_cnt == op_cnt + 1 or op_cnt == 0:
        raise ValueError('Missing or invalid operation.')

    if op_cnt > 1:
        dex = 0
        num_brackets = op_cnt
        left = True
        while num_brackets > 0:
            if left:
                statement[dex] = '(' * (num_brackets - 1) + statement[dex]
                left = False
            else:
                statement[dex] += ')'
            num_brackets -= 1
            dex += 2

    try:
        return eval(''.join(statement))
    except SyntaxError:
        raise ValueError('Invalid question.')
