from_hex = dict(zip('0123456789ABCDEF', range(16)))


def hexa(hex_str):
    decimal = 0
    for digit in hex_str:
        try:
            decimal = decimal * 16 + from_hex[digit.upper()]
        except KeyError:
            raise ValueError('Input must be a hexadecimal number!')
    return decimal
