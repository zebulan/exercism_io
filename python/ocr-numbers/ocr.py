from collections import defaultdict


def grid(nums):
    result = list()
    for num in nums:
        try:
            result.append(big[int(num)])
        except KeyError:
            raise ValueError('Invalid number.')
    return list(''.join(a) for a in zip(*result))


def number(ocr):
    nums = parse_binary_font(ocr)
    result = list()
    for key, val in nums.items():
        try:
            result.append(inverse_big[tuple(val)])
        except KeyError:
            result.append('?')
    return ''.join(result)


def parse_binary_font(txt, return_inverse=False):
    """ Return a dictionary of binary font digits. Numbers are 3x4 grids. """
    result = defaultdict(list)
    if not len(txt) == 4:
        raise ValueError('Insufficient rows. Must be 4 rows total.')

    for row in txt:
        p_key = 0                # primary key counter
        length = len(row)
        if not length % 3 == 0:  # check for consistent row length
            raise ValueError('Row size error.')
        for b in (row[a:a + 3] for a in range(0, len(row), 3)):  # chunks = 3
            result[p_key].append(b)
            p_key += 1           # number counter
    if not return_inverse:
        return result
    return result, {tuple(v): str(k) for k, v in result.items()}

big_numbers = [' _     _  _     _  _  _  _  _ ',
               '| |  | _| _||_||_ |_   ||_||_|',
               '|_|  ||_  _|  | _||_|  ||_| _|',
               '                              ']
big, inverse_big = parse_binary_font(big_numbers, return_inverse=True)
