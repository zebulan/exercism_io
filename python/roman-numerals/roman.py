def numeral(num):
    roman_numerals = ((1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'),
                      (100, 'C'), (90, 'XC'), (50, 'L'), (40, 'XL'),
                      (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I'))
    converted = list()
    for value, digit in roman_numerals:
        numeral_multiplier, num = divmod(num, value)
        if not numeral_multiplier == 0:
            converted.append(digit * numeral_multiplier)
    return ''.join(converted)
