def parse_binary(num):
    if any(digit not in '10' for digit in num):
        raise ValueError('Input must be a binary number!')

    decimal = 0
    for digit in num:
        decimal = decimal * 2 + int(digit)
    return decimal
