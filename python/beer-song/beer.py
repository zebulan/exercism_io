def song(start, finish=0):
    return '\n'.join(verse(a) for a in range(start, finish - 1, -1)) + '\n'


def verse(num):
    if num == 0:
        return "No more bottles of beer on the wall, no more bottles of beer.\n" \
               "Go to the store and buy some more, " \
               "99 bottles of beer on the wall.\n"
    elif num == 1:
        return "1 bottle of beer on the wall, 1 bottle of beer.\n" \
               "Take it down and pass it around, " \
               "no more bottles of beer on the wall.\n"
    else:
        b2 = 'bottle' if num == 2 else 'bottles'
        return "{0} bottles of beer on the wall, {0} bottles of beer.\n" \
               "Take one down and pass it around, " \
               "{1} {2} of beer on the wall.\n".format(num, num - 1, b2)
