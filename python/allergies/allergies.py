from operator import itemgetter


class Allergies():
    scores = {'eggs': 1, 'peanuts': 2, 'shellfish': 4, 'strawberries': 8,
              'tomatoes': 16, 'chocolate': 32, 'pollen': 64, 'cats': 128}

    def __init__(self, allergy_score):
        self.allergy_score = allergy_score

    def is_allergic_to(self, item):
        return True if self.allergy_score & self.scores[item] else False

    @property
    def list(self):
        allergies = list()
        for item, _ in sorted(self.scores.items(), key=itemgetter(1)):
            if self.is_allergic_to(item):
                allergies.append(item)
        return allergies
