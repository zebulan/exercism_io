def hey(what):
    what = ''.join(what.split())     # strip all unnecessary spaces
    if not what:
        return 'Fine. Be that way!'  # didn't say anything
    elif what.isupper():
        return 'Whoa, chill out!'    # yelled at bob
    elif what.endswith('?'):
        return 'Sure.'               # asked bob a question
    return 'Whatever.'               # anything else
