from string import ascii_lowercase

plaintext = ascii_lowercase
ciphertext = plaintext[::-1]


def decode(text):
    text = ''.join(a.lower() for a in text if a.isalnum())
    return text.translate(text.maketrans(plaintext, ciphertext))


def encode(text):
    text = decode(text)
    return ' '.join(text[b:b + 5] for b in range(0, len(text), 5))
