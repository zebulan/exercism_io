days = ('first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh',
        'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth')

presents = ('twelve Drummers Drumming', 'eleven Pipers Piping',
            'ten Lords-a-Leaping', 'nine Ladies Dancing',
            'eight Maids-a-Milking', 'seven Swans-a-Swimming',
            'six Geese-a-Laying', 'five Gold Rings', 'four Calling Birds',
            'three French Hens', 'two Turtle Doves')

part = 'a Partridge in a Pear Tree'
and_part = ''.join((', and ', part))
line = 'On the {} day of Christmas my true love gave to me, {}.\n'.format


def sing():
    return verses(1, 12)


def verse(num):
    if not isinstance(num, int):
        raise TypeError('Input must be integer!')
    if not 1 <= num <= 12:
        raise ValueError('Integer must be between 1 - 12')

    if num == 1:
        phrase = part
    else:
        phrase = ''.join((', '.join(presents[-num + 1:]), and_part))
    return line(days[num - 1], phrase)


def verses(start, stop):
    return '\n'.join(verse(a) for a in range(start, stop + 1)) + '\n'
