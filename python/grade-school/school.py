from collections import defaultdict


class School():
    def __init__(self, school_name):
        self.db = defaultdict(set)
        self.school_name = school_name

    def add(self, student, grade):
        self.db[grade].add(student)

    def grade(self, grade_year):
        return self.db[grade_year]

    def sort(self):
        return [(g, tuple(sorted(kids))) for g, kids in self.db.items()]
