from random import choice, shuffle
from string import ascii_uppercase, digits


def _shuffle_chars(chars):
    chars = list(chars)
    shuffle(chars)
    return ''.join(chars)


class Robot():
    def __init__(self):
        self.bot_name = None
        self.prev_names = set()

    def az_09(self, letters=True):
        chars = self._az if letters else self._09
        total = 2 if letters else 3
        return ''.join(choice(chars) for _ in range(total))

    def create_name(self):
        if self.bot_name is None:
            while True:
                self.bot_name = ''.join((self.az_09(True), self.az_09(False)))
                if self.bot_name not in self.prev_names:
                    self.prev_names.add(self.bot_name)
                    break
        return self.bot_name

    def delete_name(self):
        self.bot_name = None

    _az = _shuffle_chars(ascii_uppercase)
    _09 = _shuffle_chars(digits)
    name = property(create_name, None, delete_name)

    def reset(self):
        del self.name
