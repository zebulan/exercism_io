from functools import reduce
from operator import mul


def largest_product(num_str, slice_len):
    all_slices = slices(num_str, slice_len)
    return max(reduce(mul, chunk, 1) for chunk in all_slices)


def slices(num_str, slice_len):
    if len(num_str) < slice_len:
        raise ValueError
    return [list(map(int, num_str[a:a + slice_len]))
            for a in range(len(num_str) - slice_len + 1)]
