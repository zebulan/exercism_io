from math import ceil
from itertools import zip_longest


def encode(text):
    text = ''.join(a.lower() for a in text if a.isalnum())
    if not text:
        return text             # empty string
    length = len(text)
    line = ceil(length ** 0.5)  # length of each line
    square = (list(text[b:b+line]) for b in range(0, length, line))
    return ' '.join(''.join(c) for c in zip_longest(*square, fillvalue=''))
