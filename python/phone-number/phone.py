class Phone():
    def __init__(self, number):
        num = ''.join(a for a in number if a.isdigit())
        length = len(num)
        if length == 10:
            self.number = num
        elif length == 11 and num.startswith('1'):
            self.number = num[1:]
        else:
            self.number = '0000000000'
        self.area = self.number[:3]

    def pretty(self):
        return '({}) {}-{}'.format(self.area, self.number[3:6], self.number[6:])

    def area_code(self):
        return self.area
