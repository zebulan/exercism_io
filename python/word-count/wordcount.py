from collections import defaultdict


def word_count(text):
    totals = defaultdict(int)
    for word in text.split():
        totals[word] += 1
    return totals

# from collections import Counter
#
#
# def word_count(text):
#     return Counter(text.split())
