def transform(text):
    results = dict()
    for key, values in text.items():
        for value in values:
            results[value.lower()] = key
    return results
