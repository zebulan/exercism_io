class Luhn(object):
    def __init__(self, number):
        self.number = number

    def addends(self, double=False):
        transformed = list()
        for num in reversed(''.join(str(self.number).split())):
            current = int(num)
            if double:
                current *= 2
                current = current if current < 10 else current - 9
            double = False if double else True
            transformed.append(current)
        return transformed

    def checksum(self, double=False):
        return sum(self.addends(double)) % 10

    def create(num):
        return int(str(num) + str((10 - Luhn(num).checksum(True)) % 10))

    def is_valid(self):
        return self.checksum() == 0
