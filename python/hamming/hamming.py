def distance(one, two):
    return sum(not a == b for a, b in zip(one, two))
