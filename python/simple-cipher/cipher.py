from random import choice, randint
from string import ascii_lowercase as az


class Caesar():
    def __init__(self, key=3):
        try:
            self.key = int(key) % 26
        except ValueError:
            self.key = 3

    def decode(self, text, flip_key=False):
        key = self.key if not flip_key else self.key * -1
        text = ''.join(a.lower() for a in text if a.isalpha())
        return ''.join(az[(az.index(b) - key) % 26] for b in text)

    def encode(self, text):
        return self.decode(text, flip_key=True)


class Cipher():
    def __init__(self, key=''):
        if not key:
            self.key = ''.join(choice(az) for _ in range(randint(105, 150)))
        elif len(set(key)) == 1:
            self.key = key[0]
        else:
            self.key = key
        self.key_length = len(self.key)

    def decode(self, decode_text, positive=True):
        dex = 0
        result = list()
        for char in decode_text:
            key_dex = az.index(self.key[dex])
            key_dex = key_dex if positive else key_dex * -1
            result.append(az[(az.index(char) - key_dex) % 26])
            dex = (dex + 1) % self.key_length  # adjust key index
        return ''.join(result)

    def encode(self, encode_text):
        return self.decode(encode_text, positive=False)
