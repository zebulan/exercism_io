def slices(num_str, slice_len):
    num_len = len(num_str)
    if not 0 < slice_len <= num_len:
        raise ValueError

    return [list(map(int, num_str[a:a + slice_len]))
            for a in range(num_len - slice_len + 1)]
