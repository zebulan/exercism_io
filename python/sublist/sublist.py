EQUAL, SUBLIST, SUPERLIST, UNEQUAL = range(4)


def check_lists(a, b):
    if a == b:
        return EQUAL
    elif not a:
        return SUBLIST
    elif not b:
        return SUPERLIST
    elif is_sublist(a, b):
        return SUBLIST
    elif is_sublist(b, a):
        return SUPERLIST
    return UNEQUAL


def is_sublist(c, d):
    first = c[0]
    uno_len = len(c)
    if uno_len > len(d):
        return False
    for index, value in enumerate(d):
        if value == first and c == d[index:index + uno_len]:
            return True
    return False
