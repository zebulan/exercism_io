def calculate(max_factor, min_factor=0, minimum=True):
    results = list()
    for a in range(min_factor, max_factor + 1):
        for b in range(min_factor, a + 1):
            current = str(a * b)
            if current == current[::-1]:
                results.append((int(current), (a, b)))
    return min(results) if minimum else max(results)


def largest_palindrome(max_factor=1, min_factor=0):
    return calculate(max_factor, min_factor, minimum=False)


def smallest_palindrome(max_factor, min_factor=0):
    return calculate(max_factor, min_factor, minimum=True)
