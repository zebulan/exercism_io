def on_square(num):
    return 2 ** (num - 1)


def total_after(num):
    return sum(2 ** a for a in range(num))
