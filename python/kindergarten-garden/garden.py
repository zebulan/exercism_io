default_kids = ('Alice', 'Bob', 'Charlie', 'David', 'Eve', 'Fred',
                'Ginny', 'Harriet', 'Ileana', 'Joseph', 'Kincaid', 'Larry')
plant = {'G': 'Grass', 'C': 'Clover', 'R': 'Radishes', 'V': 'Violets'}


class Garden():
    def __init__(self, garden_map, students=default_kids):
        if not students == default_kids:
            students.sort()
        row, row2 = garden_map.split()
        self.kid_to_plants = dict(zip(students,
                                      (''.join((row[a:a+2], row2[a:a+2]))
                                       for a in range(0, len(row), 2))))

    def plants(self, student):
        return [plant[b] for b in self.kid_to_plants[student]]
