initial_sentence = 'This is {}'.format  # suffix needed
other_sentence = 'that {} {}'.format    # prefix AND suffix needed
pieces = (('lay in', 'the house that Jack built.'),
          ('ate', 'the malt'),
          ('killed', 'the rat'),
          ('worried', 'the cat'),
          ('tossed', 'the dog'),
          ('milked', 'the cow with the crumpled horn'),
          ('kissed', 'the maiden all forlorn'),
          ('married', 'the man all tattered and torn'),
          ('woke', 'the priest all shaven and shorn'),
          ('kept', 'the rooster that crowed in the morn'),
          ('belonged to', 'the farmer sowing his corn'),
          (None, 'the horse and the hound and the horn'))


def rhyme():
    poem = list()
    for a in range(len(pieces)):
        _, suffix = pieces[a]
        verse = [initial_sentence(suffix)]
        for b in range(a - 1, -1, -1):
            prefix, suffix = pieces[b]
            verse.append(other_sentence(prefix, suffix))
        poem.append('\n'.join(verse))
    return '\n\n'.join(poem)
