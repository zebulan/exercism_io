def is_leap_year(year):
    four = (year % 4 == 0)
    hundred = (year % 100 == 0)
    four_hun = (year % 400 == 0)
    return four and not hundred or four_hun
