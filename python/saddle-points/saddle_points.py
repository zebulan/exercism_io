def saddle_points(lst):
    if not all(len(a) == len(lst[0]) for a in lst):
        raise ValueError('Matrix is irregular.')

    columns = tuple(min(b) for b in zip(*lst))  # minimum for each column
    results = set()
    for r_dex, row in enumerate(lst):
        row_max = max(row)  # maximum for each row
        for c_dex, column in enumerate(row):
            if column == row_max and column == columns[c_dex]:
                results.add((r_dex, c_dex))
    return results
