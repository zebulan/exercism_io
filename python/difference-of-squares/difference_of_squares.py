def difference(num):
    return square_of_sum(num) - sum_of_squares(num)


def square_of_sum(num):
    """ Triangular Number Formula """
    return ((num * (num + 1)) / 2) ** 2


def sum_of_squares(num):
    """ Square Pyramidal Number Formula """
    return ((num * (num + 1)) * (2 * num + 1)) / 6


# def square_of_sum(num):
#     return sum(range(1, num + 1)) ** 2


# def sum_of_squares(num):
#     return sum(b ** 2 for b in range(1, num + 1))
