triangle_types = {1: 'equilateral', 2: 'isosceles', 3: 'scalene'}


class Triangle():
    def __init__(self, a, b, c):
        if not a + b > c or not b + c > a or not a + c > b or \
           any(side <= 0 for side in (a, b, c)):
            raise TriangleError
        self.a = a
        self.b = b
        self.c = c

    def kind(self):
        return triangle_types[len({self.a, self.b, self.c})]


class TriangleError(Exception):
    pass
